package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

import static ru.tsc.chertkova.tm.constant.TaskTestData.*;
import static ru.tsc.chertkova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @Test
    public void add() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
    }

    @Test
    public void addAll() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, TASK_LIST);
        Assert.assertEquals(TASK_LIST, repository.findAll());
    }

    private void addAll(ITaskRepository repository, List<Task> tasks) {
        for (Task t :
                tasks) {
            repository.add(t);
        }
    }

    @Test
    public void remove() {
        @NotNull final List<Task> list = new ArrayList<>(USER1_TASK_LIST);
        @NotNull final ITaskRepository repository = new TaskRepository();
        addAll(repository, USER1_TASK_LIST);
        @Nullable final Task removed = repository.remove(USER1_TASK2);
        Assert.assertEquals(USER1_TASK2, removed);
        list.remove(USER1_TASK2);
        Assert.assertEquals(list, repository.findAll());
        Assert.assertNull(repository.remove(USER1_TASK2));
    }

    @Test
    public void removeByUserId() {
        @NotNull final List<Task> list = new ArrayList<>(TASK_LIST);
        @NotNull final ITaskRepository repository = new TaskRepository();
        addAll(repository, TASK_LIST);
        repository.remove(USER1.getId(), USER1_TASK2);
        list.remove(USER1_TASK2);
        Assert.assertEquals(list, repository.findAll());

        repository.remove(USER1.getId(), ADMIN1_TASK1);
        Assert.assertTrue(TASK_LIST.contains(ADMIN1_TASK1));
        list.remove(ADMIN1_TASK1);
        Assert.assertNotEquals(list, repository.findAll());
    }

    @Test
    public void clear() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        addAll(repository, TASK_LIST);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void clearByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        addAll(repository, TASK_LIST);
        repository.clear(USER1.getId());
        Assert.assertFalse(repository.findAll().containsAll(USER1_TASK_LIST));
        @NotNull final List<Task> result = new ArrayList<>(TASK_LIST);
        result.removeAll(USER1_TASK_LIST);
        Assert.assertEquals(repository.findAll(), result);
    }

    @Test
    public void findAll() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, TASK_LIST);
        Assert.assertEquals(TASK_LIST, repository.findAll());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, TASK_LIST);
        Assert.assertEquals(ADMIN1_TASK_LIST, repository.findAll(ADMIN1.getId()));
    }

    @Test
    public void findById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, TASK_LIST);
        Assert.assertEquals(
                repository.findById(USER1.getId(), USER1_TASK2.getId()),
                USER1_TASK2
        );
        Assert.assertNull(repository.findById(USER1.getId(), ADMIN1_TASK2.getId()));
    }

    @Test
    public void removeById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, TASK_LIST);
        Assert.assertEquals(
                repository.removeById(ADMIN1.getId(), ADMIN1_TASK2.getId()),
                ADMIN1_TASK2
        );
        @NotNull final List<Task> list = new ArrayList<>(TASK_LIST);
        list.remove(ADMIN1_TASK2);
        Assert.assertEquals(repository.findAll(), list);
        Assert.assertNull(repository.removeById(USER1.getId(), ADMIN1_TASK2.getId()));
        Assert.assertEquals(repository.findAll(), list);
    }

}
