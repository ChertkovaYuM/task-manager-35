package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.repository.ProjectRepository;
import ru.tsc.chertkova.tm.repository.TaskRepository;
import ru.tsc.chertkova.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static ru.tsc.chertkova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService service = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();


    @Before
    public void setUp() {
        for (User u :
                USER_LIST) {
            userRepository.add(u);
        }
    }

    @After
    public void tearDown() {
        userRepository.clear();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER_LIST, service.findAll());
    }

    @Test
    public void clear() {
        service.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void remove() {
        @NotNull final List<User> list = new ArrayList<>(userRepository.findAll());
        @Nullable final User removed = service.remove(USER1);
        Assert.assertEquals(USER1, removed);
        list.remove(USER1);
        Assert.assertEquals(list, userRepository.findAll());
        Assert.assertNull(service.remove(null));
        Assert.assertNull(service.remove(USER1));
    }

    @Test
    public void findById() {
        Assert.assertEquals(
                service.findById(USER1.getId()),
                USER1
        );
        thrown.expect(IdEmptyException.class);
        service.findById(null);
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(
                service.findByLogin(USER1.getLogin()),
                USER1
        );
        thrown.expect(LoginEmptyException.class);
        service.findByLogin(null);
    }

    @Test
    public void removeById() {
        Assert.assertTrue(service.findAll().contains(USER1));
        service.removeById(USER1.getId());
        Assert.assertFalse(service.findAll().contains(USER1));
    }

    @Test
    public void create() {
        userRepository.clear();
        service.create(USER1.getLogin(), "password"
        );
        Assert.assertNotNull(userRepository.findByLogin(USER1.getLogin()));
    }

    @Test
    public void createWithEmail() {
        userRepository.clear();
        service.create(USER1.getLogin(), "password", USER1.getEmail()
        );
        @Nullable final User created = userRepository.findByLogin(USER1.getLogin());
        if (created == null) fail("Error!");
        Assert.assertEquals(USER1.getEmail(), created.getEmail());
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
    }

    @Test
    public void createWithRole() {
        userRepository.clear();
        service.create(USER1.getLogin(), "password", USER1.getRole()
        );
        @Nullable final User created = userRepository.findByLogin(USER1.getLogin());
        if (created == null) fail("Error!");
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
        Assert.assertEquals(USER1.getRole(), created.getRole());
    }

    @Test
    public void updateById() {
        userRepository.clear();
        @NotNull final User actual = new User();
        actual.setId(ADMIN1.getId());
        userRepository.add(actual);
        service.updateUser(
                actual.getId(),
                ADMIN1.getFirstName(),
                ADMIN1.getLastName(),
                ADMIN1.getMiddleName()
        );
        assertThat(service.findById(actual.getId())).isEqualToIgnoringGivenFields(
                ADMIN1, "passwordHash", "role"
        );
    }

    @Test
    public void updatePasswordById() {
        userRepository.clear();
        @NotNull final User user1 = new User();
        user1.setId(USER1.getId());
        userRepository.add(user1);
        @NotNull final User user2 = new User();
        user2.setId(USER2.getId());
        userRepository.add(user2);
        service.setPassword(USER1.getId(), "password");
        service.setPassword(USER2.getId(), "password");
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
    }

    @Test
    public void lockUserByLogin() {
        userRepository.clear();
        @NotNull final User user1 = new User();
        user1.setLogin(USER1.getLogin());
        user1.setLocked(false);
        userRepository.add(user1);
        service.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(user1.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        userRepository.clear();
        @NotNull final User user1 = new User();
        user1.setLogin(USER1.getLogin());
        user1.setLocked(true);
        userRepository.add(user1);
        service.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(user1.getLocked());
    }

    @Test
    public void removeByLogin() {
        Assert.assertTrue(userRepository.findAll().contains(USER1));
        service.removeByLogin(USER1.getLogin());
        Assert.assertFalse(userRepository.findAll().contains(USER1));
    }

}
