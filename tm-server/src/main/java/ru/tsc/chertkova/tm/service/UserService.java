package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.exception.AbstractException;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginExistsException;
import ru.tsc.chertkova.tm.exception.user.PasswordEmptyException;
import ru.tsc.chertkova.tm.exception.user.RoleEmptyException;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.util.HashUtil;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @Nullable
    private final IPropertyService propertyService;

    @Nullable
    private final IProjectRepository projectRepository;

    @Nullable
    private final ITaskRepository taskRepository;

    public UserService(@Nullable final IPropertyService propertyService,
                       @Nullable final IUserRepository repository,
                       @Nullable final IProjectRepository projectRepository,
                       @Nullable final ITaskRepository taskRepository) {
        super(repository);
        this.propertyService = propertyService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Nullable
    @Override
    public User remove(@Nullable final User model) throws AbstractException {
        if (model == null) throw new UserNotFoundException();
        final User user = super.remove(model);
        if (user == null) throw new UserNotFoundException();
        @Nullable final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return remove(findByLogin(login));
    }

    @Nullable
    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        @Nullable final User user = createUser(login, password);
        return repository.add(user);
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @Nullable final User user = createUser(login, password);
        user.setEmail(email);
        return repository.add(user);
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final User user = createUser(login, password);
        user.setRole(role);
        return repository.add(user);
    }

    @NotNull
    public User createUser(@Nullable final String login, @Nullable final String password) {
        @NotNull final User user = new User();
        user.setRole(Role.USUAL);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @Nullable
    @Override
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        return user;
    }

    @Nullable
    @Override
    public User updateUser(@Nullable final String userId, @Nullable final String firstName,
                           @Nullable final String lastName, @Nullable final String middleName) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = findById(userId);
        if (firstName == null || firstName.isEmpty()) throw new NameEmptyException();
        if (lastName == null || lastName.isEmpty()) throw new NameEmptyException();
        if (middleName == null || middleName.isEmpty()) throw new NameEmptyException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return null;
    }

    @Nullable
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(false);
        return user;
    }

}
