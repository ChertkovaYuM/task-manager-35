package ru.tsc.chertkova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.endpoint.*;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.endpoint.*;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.repository.ProjectRepository;
import ru.tsc.chertkova.tm.repository.TaskRepository;
import ru.tsc.chertkova.tm.repository.UserRepository;
import ru.tsc.chertkova.tm.service.*;
import ru.tsc.chertkova.tm.util.DateUtil;
import ru.tsc.chertkova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final CalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint();

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(userEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        final String host = "localhost";
        final String port = "8080";
        final String name = endpoint.getClass().getSimpleName();
        final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run() {
        initPID();
        initData();
        loggerService.info("** TASK MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER STOPPED **");
        backup.stop();
    }

    public void initData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);

        taskService.add(new Task("task vot", Status.IN_PROGRESS, DateUtil.toDate("04.10.2019")));
        taskService.add(new Task("task kak-to", Status.NOT_STARTED, DateUtil.toDate("04.12.2019")));
        taskService.add(new Task("task tak", Status.COMPLETED, DateUtil.toDate("04.9.2019")));
        projectService.add(new Project("project no", Status.COMPLETED, DateUtil.toDate("04.10.2018")));
        projectService.add(new Project("project nikak", Status.IN_PROGRESS, DateUtil.toDate("04.10.2021")));
        projectService.add(new Project("project inache", Status.NOT_STARTED, DateUtil.toDate("04.10.2020")));
    }

}
