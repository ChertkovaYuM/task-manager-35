package ru.tsc.chertkova.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;
import ru.tsc.chertkova.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public ProjectListRequest(@Nullable String token, @Nullable Sort sort) {
        super(token);
        this.sort = sort;
    }

}
